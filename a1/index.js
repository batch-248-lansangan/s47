console.log("Hi, B248!");
console.log(document);//result - html document
console.log(document.querySelector("#txt-first-name")); 
console.log(document.querySelector("#txt-last-name")); 


const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

function updateFullName(event) {
  console.log(event);
  console.log(event.target);
  console.log(event.target.value);
  spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
}

txtFirstName.addEventListener('keyup', updateFullName);

txtLastName.addEventListener('keyup', updateFullName);
